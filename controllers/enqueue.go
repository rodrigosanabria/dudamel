package controllers
import ("fmt"
"net/http"
lib "dudamel/lib"
)


func Enqueue(w http.ResponseWriter, req *http.Request) {
	q := req.URL.Query()
	eventType := q.Get("eventType")
		event := lib.Event{EventType: eventType, Payload: "hello"} 
		lib.EventQueue.Add(&event)
		fmt.Fprintf(w, "hello\n")
	}