package main
import (
    "net/http"
	controllers "dudamel/controllers"
	lib "dudamel/lib"
)
func main() {

	lib.Init()

/* 	done := make(chan bool)
	lib.InitWorkerPool("test",5)

	lib.MyWorkerPool.StartPool(done) */

	http.HandleFunc("/", controllers.Enqueue)

	http.ListenAndServe(":8090", nil)
}