package lib

import ("fmt"
	"sync")

type Work interface{
	Task() string
}


type WorkerPool struct {
	works chan Work
	workerType string
	size int
	Wg sync.WaitGroup
	results chan string
	done chan bool
}

func (wp *WorkerPool) StartPool() {
	wp.Wg.Add(wp.size)
	for i := 0; i < wp.size; i++ {
		go StartWorker(i,wp.works,wp.results, wp.done, &wp.Wg)
	}
	go func (wp *WorkerPool) {
	for result := range wp.results {
		wp.Wg.Add(1)
		go func (result string) {
			fmt.Println(result)
			wp.Wg.Done()
		}(result)
	}
}(wp)
}

func (wp *WorkerPool) Run(work Work) {
	wp.works <-work
}

func (wp *WorkerPool) ClosePool() {
	for i := 0; i < wp.size; i++ {
		wp.done<-true
	}
	close(wp.works)
	close(wp.results)
	close(wp.done)
	wp.Wg.Wait()

}

func StartWorker(workerIndex int, workChan chan Work, results chan <-string, done chan bool, wp *sync.WaitGroup) {
	for {
		select { 
		case work := <- workChan:
			fmt.Println("lets work",workerIndex)
			results <- work.Task()
		case <- done:
			wp.Done()
			return
		
	}
	}
}


func NewWorkerPool (workerType string,size int) *WorkerPool{
	done := make(chan bool)
	wp := WorkerPool{
		works: make(chan Work),
		workerType: workerType,
		size: size,
		done: done,
		results: make(chan string)}
		wp.StartPool()
	return &wp
}
