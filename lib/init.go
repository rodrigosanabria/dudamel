package lib

import ("fmt"
		"time")

var MyWorkerPool WorkerPool 
var Pools map[string]*WorkerPool
var EventQueue Queue

type AsyncTask struct {

}

func (at *AsyncTask) Task() string {
	time.Sleep(time.Second*1)
	return "Ok"
}

func eventDispatch(event *Event) {

	pool,ok := Pools[event.EventType]
	if (ok == true) {
		fmt.Println("Run type",event.EventType)
		asyncTask := &AsyncTask{} 
		pool.Run(asyncTask)
	} else {
		fmt.Println("This event is not supported.")
	}

}

func Init() {
	EventQueue = Queue{}
	Pools =  map[string]*WorkerPool{
		"high": NewWorkerPool("high",10),
		"medium": NewWorkerPool("medium",8),
		"low": NewWorkerPool("low",5)}

	go func (queue *Queue) {
	for {
		fmt.Println("Current Queue", len(queue.events), time.Now())
		event,err := queue.Remove()
		if (err != nil) {
			time.Sleep(time.Second*10)
		} else {
		eventDispatch(event)
		}
	}
}(&EventQueue)
}


