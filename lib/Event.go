package lib


type Event struct {
	EventType string
	Payload any
}