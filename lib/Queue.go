package lib

import ("fmt")

type Queue struct {
	events []*Event
}

func (q *Queue) Add(event *Event) *Queue {
	fmt.Println("Adding to the queue")
	q.events = append(q.events,event)
	return q
}

func (q *Queue) Remove() (*Event,error) {
	if len(q.events) == 0 {
		return &Event{}, fmt.Errorf("Queue is empty")
	}
	event := q.events[0]
	q.events = q.events[1:]
	return event, nil

}